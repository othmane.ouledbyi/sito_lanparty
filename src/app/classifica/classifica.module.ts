import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { ClassificaDashboardComponent } from './classifica-dashboard/classifica-dashboard.component';
import { ClassificaRoutingModule } from './classifica-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ClassificaDashboardComponent
  ],
  imports: [
    CommonModule,
    ClassificaRoutingModule, MatIconModule, FormsModule,
  ]
})
export class ClassificaModule { }
