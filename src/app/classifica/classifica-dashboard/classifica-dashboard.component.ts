import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClassificaService } from './ClassificaService';

@Component({
  selector: 'app-classifica-dashboard',
  templateUrl: './classifica-dashboard.component.html',
  styleUrls: ['./classifica-dashboard.component.scss']
})
export class ClassificaDashboardComponent implements OnInit {
  data = [];
  top3 = [];
  reveal = false;
  selectedClass: string;

  constructor(private classificaService: ClassificaService) {}

  ngOnInit() {
    this.fetchData();
    this.fetchTop3();
  }

  fetchData() {
    this.classificaService.getAllData().subscribe(response => {
      this.data = response;
      setTimeout(() => {
        this.reveal = true;
      }, 1000); // Delay for reveal effect
    });
  }

  fetchTop3() {
    this.classificaService.getTop3().subscribe(response => {
      this.top3 = response;
      // Aggiungi la classe 'reveal' dopo aver ottenuto i dati delle top 3 classi
      this.addRevealClass();
    });
  }

  addRevealClass() {
    // Aggiungi la classe 'reveal' all'elemento dopo aver ottenuto i dati delle top 3 classi con un ritardo di 1 secondo
    const element = document.querySelector('.scoreboard__podiums');
    if (element) {
      setTimeout(() => {
        element.classList.add('reveal');
      }, 100);
    }
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      const classId = form.value.classId;
      const score = form.value.score;

      this.addScore(classId, score);
      form.reset();
    }
  }

  addScore(id: string, score: number) {
    this.classificaService.addScore(id, score).subscribe(response => {
      console.log(response);
      this.fetchData(); // Refresh the data after adding score
      this.fetchTop3(); // Refresh the top 3 after adding score
    }, error => {
      console.error(error);
    });
  }
}
