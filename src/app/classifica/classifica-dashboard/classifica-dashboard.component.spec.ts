import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificaDashboardComponent } from './classifica-dashboard.component';

describe('ClassificaDashboardComponent', () => {
  let component: ClassificaDashboardComponent;
  let fixture: ComponentFixture<ClassificaDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClassificaDashboardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ClassificaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
