import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassificaService {
  private baseUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getAllData(): Observable<any> {
    return this.http.get(`${this.baseUrl}/getAllData`);
  }

  getTop3(): Observable<any> {
    return this.http.get(`${this.baseUrl}/getTop3`);
  }

  addScore(id: string, score: number): Observable<any> {
    return this.http.post(`${this.baseUrl}/add`, null, { params: { id, score: score.toString() } });
  }
}
