import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassificaDashboardComponent } from './classifica-dashboard/classifica-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: ClassificaDashboardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassificaRoutingModule { }
