import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { TimerDashboardComponent } from '../timer/timer-dashboard/timer-dashboard.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { HomeRoutingModule } from './home-routing.module';


@NgModule({
  declarations: [
    HomeDashboardComponent, TimerDashboardComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule, MatIconModule,
  ]
})
export class HomeModule { }

// ng generate module timer --routing
// ng generate component timer/timer-dashboard
// ng add @angular/material
// ng g @angular/material:nav mainNav
