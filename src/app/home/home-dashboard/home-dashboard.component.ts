import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.scss']
})
export class HomeDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  setActiveCard(event: MouseEvent): void {
    const target = (event.target as HTMLElement).closest(".card");
    const cards = document.querySelectorAll(".card");
    cards.forEach(card => {
      card.classList.remove("active");
    });
    target?.classList.add("active");
  }

}
