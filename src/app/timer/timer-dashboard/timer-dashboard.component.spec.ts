import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimerDashboardComponent } from './timer-dashboard.component';

describe('TimerDashboardComponent', () => {
  let component: TimerDashboardComponent;
  let fixture: ComponentFixture<TimerDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimerDashboardComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
