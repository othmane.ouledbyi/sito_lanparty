import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-timer',
  templateUrl: './timer-dashboard.component.html',
  styleUrls: ['./timer-dashboard.component.css']
})
export class TimerDashboardComponent implements OnInit, OnDestroy {
  private endDate: Date = new Date('2024-06-11T08:15:00');
  private subscription: Subscription = new Subscription();
  public days: string = '00';
  public hours: string = '00';
  public minutes: string = '00';
  public seconds: string = '00';

  ngOnInit(): void {
    this.updateTimeLeft();
    this.subscription = interval(1000).subscribe(() => {
      this.updateTimeLeft();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private updateTimeLeft(): void {
    const now = new Date().getTime();
    const diff = this.endDate.getTime() - now;

    if (diff < 0) {
      this.days = '00';
      this.hours = '00';
      this.minutes = '00';
      this.seconds = '00';
    } else {
      const days = Math.floor(diff / (1000 * 60 * 60 * 24));
      const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((diff % (1000 * 60)) / 1000);

      this.days = ("0" + days).slice(-2);
      this.hours = ("0" + hours).slice(-2);
      this.minutes = ("0" + minutes).slice(-2);
      this.seconds = ("0" + seconds).slice(-2);
    }
  }
}
