import {
  MAT_SELECT_CONFIG,
  MAT_SELECT_SCROLL_STRATEGY,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY,
  MAT_SELECT_TRIGGER,
  MatSelect,
  MatSelectChange,
  MatSelectModule,
  MatSelectTrigger,
  matSelectAnimations
} from "./chunk-BL4HZTKE.js";
import {
  MatError,
  MatFormField,
  MatHint,
  MatLabel,
  MatPrefix,
  MatSuffix
} from "./chunk-FTMRCM7E.js";
import "./chunk-6WRAQXGT.js";
import "./chunk-CTZYTDOB.js";
import "./chunk-SM63XUJ7.js";
import "./chunk-XPH7R4LP.js";
import "./chunk-DW73JZZ3.js";
import "./chunk-RXTD4AJZ.js";
import {
  MatOptgroup,
  MatOption
} from "./chunk-Q5UPR6YR.js";
import "./chunk-237XUEJ5.js";
import "./chunk-DM37FAVG.js";
import "./chunk-FPBGV6M7.js";
import "./chunk-WXR3NZQD.js";
import "./chunk-EWYMFSQW.js";
import "./chunk-SAVZWRQP.js";
import "./chunk-SG3BCSKH.js";
import "./chunk-SAVXX6OM.js";
import "./chunk-PQ7O3X3G.js";
import "./chunk-IEMOZLTW.js";
export {
  MAT_SELECT_CONFIG,
  MAT_SELECT_SCROLL_STRATEGY,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER,
  MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY,
  MAT_SELECT_TRIGGER,
  MatError,
  MatFormField,
  MatHint,
  MatLabel,
  MatOptgroup,
  MatOption,
  MatPrefix,
  MatSelect,
  MatSelectChange,
  MatSelectModule,
  MatSelectTrigger,
  MatSuffix,
  matSelectAnimations
};
//# sourceMappingURL=@angular_material_select.js.map
